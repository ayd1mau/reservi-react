import { Component, OnInit } from '@angular/core';

import { LoginService} from '../services/login.service'
import { Registro } from '../models/registro.interface';

import { UploadService } from '../services/upload.service'
import { stringify } from 'querystring';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  // VARIABLES PARA MOSTRAR EL DIV DEL FORMULARIO PARA CADA USUARIO/PROFESIONAL
  clientStatus: boolean = false;
  workStatus: boolean = false;

  // VARIABLES PARA MOSTRAR LAS NOTIFICACIONES
  acceptNotificacion: boolean = false;
  errorNotificacion1: boolean = false;


  // VARIABLES PARA RECOLECTAR INFORMACION
  name: string = "";
  lastname: string = "";
  username: string = "";
  numberPhone: number;
  direction: string = "";
  email: string = "";
  password: string = "";

  // VARIABLES PARA EL MENSAJE
  messageErr: string = "";
  mesSolution: string = "";

  // VARIABLE PARA GUARDAR EL ARCHIVO
  toFile;

  constructor(public registro:LoginService, private uploadService: UploadService) { }

  ngOnInit(): void {
  }


  MostrarFormularioCliente(): void{
    // MUESTRO EL FORMULARIO PARA CLIENTE
    if(!this.clientStatus)
      this.clientStatus = true;

    this.workStatus = false;
  }

  MostrarFormularioTrabador(): void{
    // LLAMO EL FORMULARIO BASE
    this.MostrarFormularioCliente(); 

    // MUESTRO EL FORMULARIO PARA EL PROFESIONAL
    if(!this.workStatus)
      this.workStatus = true;
  }


  Verificar_Datos(): void{
    //let tipo=(this.clientStatus)?"cliente":"proveedor";
    let tipo = "";
    let ruta = "";

    // SE ASIGNA UN TIPO Y SE VERIFICA EL FORMATO PDF
    if(!this.workStatus){
      tipo = "cliente";
    }else{
      tipo = "proveedor";
      if(this.toFile == null){
        this.messageErr= "Tu registro no se pudo completar, ";
        this.mesSolution = " Envie su curriculum!!";

        this.errorNotificacion1=true;
      }
      // ***** VERIFICO QUE SEA PDF ******
      if(!this.isPDF(this.toFile.item(0).type)){
        this.errorNotificacion1 = true;
        return;
      }
      // RUTA DONDE ESTA ALMACENADO
      ruta ='https://bucket-reservi.s3.us-east-2.amazonaws.com/'+ this.submit();
    }
 
    // SE HACE EL INGRESA EL NUEVO USUARIO
    this.registro.postResgistro(this.name,this.lastname,this.username,this.numberPhone,
      this.direction,this.email,this.password,tipo,ruta).subscribe((res:Registro)=>{

        this.acceptNotificacion = true;
        this.Limpiar_Variables();


      },
      error=>{
        if(error.error.statusCode == 400)
        { 
          this.acceptNotificacion = true;
          this.Limpiar_Variables();
        }else{
          this.messageErr= "Tu registro no se pudo completar, ";
          this.mesSolution = " revise sus datos!!";
          console.log(error.error.statusCode)
          this.errorNotificacion1=true;
        }
        //console.log(error.error.statusCode)
        //this.errorNotificacion1=true;
        //this.Limpiar_Variables();
      });

      this.acceptNotificacion = false;
      this.errorNotificacion1 = false;

  }

  // METODO PARA OBTENER EL ARCHIVO
  onChange(event){
    this.toFile = event.target.files;
  }

  // METODO PARA VERIFICAR QUE ES UN PDF
  public isPDF(type: string): boolean{
    //const file = this.toFile.item(0);

    if(! (type === 'application/pdf')){
      this.messageErr = "El formato de su archivo no es compatible";
      this.mesSolution = "Envie un archivo .pdf!";
      return false;
    }
    return true;
  }

  // METODO PARA SUBIR CURRICULUM AL BUCKET
  private submit(): string{
    const file = this.toFile.item(0);
    const nombre = this.numHex()+file.name;
    this.uploadService.fileUpload(file,nombre);
    return nombre;
  } 

  // METODO PARA GENERAR UN NUMERO RANDOM HEX
  private numHex(): string{
    const genRanHex = size => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');  
    return genRanHex(16);
  }

  // METODO PARA VALIDAR QUE SOLO INGRESE NUMEROS PARA EL TELEFONO
  validarNumero(event: any){
    const pattern = /[0-9\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  validaNumero(input: string): boolean{
    const pattern = /[0-9\ ]/;
    if (!pattern.test(input)) 
      return false
    return true;
  }


  // METODO PARA LIMPIAR VARIABLES
  Limpiar_Variables(): void{
    this.name = "";
    this.lastname = ""; 
    this.username = "";
    this.numberPhone = 0; 
    this.direction = ""; 
    this.email = "";
    this.password = "";
  }

}
