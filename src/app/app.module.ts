import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { MarkdownModule } from "ngx-markdown";

import { RouterModule, Routes } from "@angular/router";

import { NavComponent } from "./nav/nav.component";
import { ArticlesComponent } from "./articles/articles.component";
import { ArticleComponent } from "./article/article.component";
import { CategoryComponent } from "./category/category.component";
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { VistaListadoClientesComponent } from './vista-listado-clientes/vista-listado-clientes.component';
import { ListadoProveedorComponent } from './listado-proveedor/listado-proveedor.component';
import { AdministradorComponent } from './administrador/administrador.component';
import { SolicitarAyudaComponent } from './solicitar-ayuda/solicitar-ayuda.component';
import { EnviarCotizacionComponent } from './enviar-cotizacion/enviar-cotizacion.component';
import { ForgotComponent } from './forgot-password/forgot-password.component';

import { PdfViewerModule } from 'ng2-pdf-viewer'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ToastComponent } from './toast/toast.component';
import { VistaProveedorComponent } from './vista-proveedor/vista-proveedor.component';
import { EdituserAdminComponent } from './edituser-admin/edituser-admin.component';



const appRoutes: Routes = [
  { path: "", component: ArticlesComponent },
  { path: "article/:id", component: ArticleComponent },
  { path: "category/:id", component: CategoryComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ArticlesComponent,
    ArticleComponent,
    CategoryComponent,
    LoginComponent,
    RegistroComponent,
    VistaListadoClientesComponent,
    ListadoProveedorComponent,
    AdministradorComponent,
    SolicitarAyudaComponent,
    EnviarCotizacionComponent,
    ToastComponent,
    ForgotComponent,
    VistaProveedorComponent,
    EdituserAdminComponent
  ],
  imports: [
    FormsModule,
    MarkdownModule.forRoot(),
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    BrowserModule,
    AppRoutingModule,
    GraphQLModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    PdfViewerModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
