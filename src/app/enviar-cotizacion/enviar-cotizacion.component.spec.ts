import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Router,ActivatedRoute} from '@angular/router';
import { EnviarCotizacionComponent } from './enviar-cotizacion.component';
import {ProveedorService} from '../services/proveedor.service';
import { LoginService }  from '../services/login.service';
<<<<<<< HEAD
=======
import {Clientes} from '../models/Clientes.interface';
>>>>>>> d25ef492c95e0f7d12ca7b27ba1c40d8dc3e12ce
describe('EnviarCotizacionComponent', () => {
  let app: EnviarCotizacionComponent;
  let router: Router;
  let act:ActivatedRoute;
  let pro:ProveedorService;
<<<<<<< HEAD
  let loginser:LoginService;
  beforeEach(async(() => {
    app=new EnviarCotizacionComponent(router,act,pro,loginser);
=======
  let log:LoginService;
  let cli:Clientes;
  beforeEach(async(() => {
    app=new EnviarCotizacionComponent(router,act,pro,log);
>>>>>>> d25ef492c95e0f7d12ca7b27ba1c40d8dc3e12ce
  }));

  it('Preuba 1 correcta para campos del registro',async(() => {
    let form={
      value:{comentario:'comentario para test'}
    }
    expect(app.onRegistrar(form)).toEqual(false);
  }));
/*  it('Preuba 2 correcta para campos del registro',async(() => {
    let form={
      value:{
        precio:1000.00,
        comentario:'comentario para test',
        fecha:'2020-10-28'
      }
    }
    expect(app.onRegistrar(form)).toEqual(true);
  }));*/

  //prueba para saber si trajo la solicitud
  it('Se ingreso bien la solicitud',async(() => {
    let solic={
        titulo:'test',
        descripcion:'comentario para test'
      };    
    app.solicitud=solic;
    expect(app.solicitud.titulo).toContain('test');
  }));

  it('El proveedor tiene todos los datos',async(() => {     
    cli={
      user:
      {
        id: '1',
        username: 'user',
        email: 'user@gmail.com',
        provider: 'test',
        confirmed: 'true',
        blocked: 'false'
      }
    }
    expect(app.revisionProveedor(cli)).toBe(true);
  }));

  it('El proveedor no esta confirmado',async(() => {     
    cli={
      user:
      {
        id: '1',
        username: 'user',
        email: 'user@gmail.com',
        provider: 'test',
        confirmed: 'false',
        blocked: 'false'
      }
    }
    expect(app.isConfirmed(cli)).toBe(false);
  }));
  

});
