import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {ProveedorService} from '../services/proveedor.service';
import { LoginService }  from '../services/login.service';
import {Clientes} from '../models/Clientes.interface';
@Component({
  selector: 'app-enviar-cotizacion',
  templateUrl: './enviar-cotizacion.component.html',
  styleUrls: ['./enviar-cotizacion.component.css']
})
export class EnviarCotizacionComponent implements OnInit {
  user = {};
  aux:Clientes=null;
  
  constructor(private router: Router,private activateRoute:ActivatedRoute,
              private proveedorService:ProveedorService,public login:LoginService) { 
                //this.aux=this.login.getUseraux();
              }

  ngOnInit() {
    this.aux=this.login.getUseraux();
    this.getSolicitud();    
    //console.log('/////////////////////////',this.aux);
  }
  onRegistrar(form) {
    if(form.value.comentario==undefined || form.value.precio==undefined  || form.value.fecha==undefined){
      alert('Faltan campos.')
      return false;
    }
    form.value.solicitud=this.solicitud;
    form.value.estado='espera';
    form.value.user=this.aux.user;
    console.log('*******',form.value)
    this.proveedorService.setCotizacion(form.value).subscribe(
      (res) => {
        console.log("registrado", res);
        alert("¡Cotización enviada!");
        this.router.navigateByUrl("/listado-proveedor");
      },
      (err) => {
        console.error();
        alert("Error al enviar cotización.");
      }
    );

    return true;
  }
  solicitud:any=[];
  getSolicitud(){
    
    this.activateRoute.paramMap.subscribe(params  =>{
      const id = params.get('id');
      console.log('ID ',id )
      this.proveedorService.buscarUno(id).subscribe(
        res=>{          
          this.solicitud=res;
          console.log('solicitud: ',this.solicitud.titulo);
        },
        err=>{
          console.log(err);
          alert(err.message);
        }
      );
    });
  }
  revisionProveedor(cli:Clientes):boolean{
    if(cli.user.id!=undefined || cli.user.username!=undefined || cli.user.email!=undefined ||
      cli.user.confirmed!=undefined ){
        return true;
      }
      return false;
  }

  isConfirmed(cli:Clientes):boolean{
    if(cli.user.confirmed=='true')
      return true;
    return false;
  }
}
