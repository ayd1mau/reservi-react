import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaProveedorComponent } from './vista-proveedor.component';

describe('VistaProveedorComponent', () => {
  let component: VistaProveedorComponent;
  let fixture: ComponentFixture<VistaProveedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaProveedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
