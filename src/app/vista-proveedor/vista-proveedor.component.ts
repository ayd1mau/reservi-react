import { Component, OnInit } from '@angular/core';

import { LoginService}  from '../services/login.service';
import {AceptarCotizacionService}  from '../services/aceptar-cotizacion.service';
import {SolicitudService} from '../services/solicitud.service';
import { Router } from '@angular/router';
import { login } from '../models/login.interface';
import {cotizacion}  from '../models/cotizaciones.interface';
import {Solicitud} from '../models/solicitud.interface';
import { element } from 'protractor';


@Component({
  selector: 'app-vista-proveedor',
  templateUrl: './vista-proveedor.component.html',
  styleUrls: ['./vista-proveedor.component.css']
})
export class VistaProveedorComponent implements OnInit {
  private usuario:login;
  public usu:String; 
  estado:String;
  id_cotizacion:String;
  public verSolicitud:Boolean;
  constructor(public listado:LoginService,public solicitudCliente:SolicitudService,public servicioCotizacion:AceptarCotizacionService ,private route:Router) {

    this.usuario=this.listado.getUseraux();
    this.usu="";
  
    
    this.listado.getCotizacion(this.usuario.user.email).subscribe((res)=>{
  //this.listado.getCotizacion(this.usuario).subscribe((res)=>{
     console.log(res);
     for(let re in res){
      this.cotizacioness.push(res[re]);
     }  
    
     console.log("----------------------------------------");
     console.log(this.cotizacioness);
     
    
     })
   }

  
   cotizacioness:cotizacion[] =[];
 
   ngOnInit() {
 
   
 
 }
 
 modificarEstado(tipo:number,id_solicitud:string,id_c:string){
  if(tipo===1){
    this.estado="aceptado";
    this.id_cotizacion=id_c;
   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.solicitudCliente.putsoli(id_solicitud,this.estado).subscribe((res)=>{
      window.location.reload();
    });

    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
    
  }else if(tipo===2){
    this.estado="rechazado";
    this.id_cotizacion=id_c;

    this.solicitudCliente.putsoli(id_solicitud,this.estado).subscribe((res)=>{
      window.location.reload();
    });

   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
  }else if(tipo===3){

    this.estado="contraofertado";
    this.id_cotizacion=id_c;

    this.solicitudCliente.putsoli(id_solicitud,this.estado).subscribe((res)=>{
      window.location.reload();
    });

   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
  }
 
  
}
public contador:number=0;
verSol(){

if(this.contador==0){
  this.verSolicitud=true;
  this.contador=1;
}else{
  this.verSolicitud=false;
  this.contador=0;
}

}
 

}
