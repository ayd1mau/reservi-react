import { Component, OnInit } from '@angular/core';
import { LoginService} from '../services/login.service'
import { SolicitarAyudaService} from '../services/solicitar-ayuda.service';
import {ayuda_body} from  '../models/ayuda.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-solicitar-ayuda',
  templateUrl: './solicitar-ayuda.component.html',
  styleUrls: ['./solicitar-ayuda.component.css']
})
export class SolicitarAyudaComponent implements OnInit {

  constructor(public loginService: LoginService,public  solicitarAyuda:SolicitarAyudaService) { }
  
 
 
nombre:String="";
apellido:String="";
correo:String="";
telefono:number=0;
descripcion:String="";


  
  ngOnInit() {
    

  }

  info:ayuda_body;


  enviar_datos():boolean{

   this.info ={nombre:this.nombre,
    apellido:this.apellido,
    email:this.correo,
    phone:this.telefono,
    descripcion_problema:this.descripcion,
    estado_duda:"enproceso"
  };
  


  if(this.nombre!="" && this.apellido!="" && this.correo!="" && this.telefono>0 && this.descripcion!=""){
    
    this.solicitarAyuda.agregarAyuda(this.nombre,this.apellido,this.correo,this.telefono,this.descripcion).subscribe((res)=>{
      alert("Tu solicitud fue enviada con exito");
    });

    return true;
  }else{
    alert("Debe llenar todos los campos");
    return false;
  }
    
   
  }
}
