import { Component, OnInit } from '@angular/core';
import { LoginService}  from '../services/login.service';
import {AceptarCotizacionService}  from '../services/aceptar-cotizacion.service';
import {SolicitudService} from '../services/solicitud.service';
import { Router } from '@angular/router';
import { login } from '../models/login.interface';
import {cotizacion}  from '../models/cotizaciones.interface';
import {Solicitud} from '../models/solicitud.interface';
import { element } from 'protractor';

@Component({
  selector: 'app-vista-listado-clientes',
  templateUrl: './vista-listado-clientes.component.html',
  styleUrls: ['./vista-listado-clientes.component.css']
})


 

export class VistaListadoClientesComponent implements OnInit {
public usuario:login;
public usu:String; 
estado:String;
id_cotizacion:String;
public cotizaiones:Boolean;
constructor(public listado:LoginService,public solicitudCliente:SolicitudService,public servicioCotizacion:AceptarCotizacionService ,private route:Router) { 

  this.usuario=this.listado.getUseraux();
  this.usu="";

  

  this.solicitudCliente.getSolicitud(this.usuario.user.email).subscribe((res)=>{
 // this.solicitudCliente.getSolicitud("marcosasantosa1998@gmail.com").subscribe((res)=>{
 //this.listado.getCotizacion(this.usuario).subscribe((res)=>{
   console.log(res);
   for(let re in res){
    this.solicitudes.push(res[re]);
   }  
  
   console.log("----------------------------------------");
   console.log(this.solicitudes);
   
  
   })

  }
  

  
  solicitudes:Solicitud[] =[];
 
  ngOnInit() {

  

}

modificarEstado(tipo:number,id_solicitud:String,id_c:String){
  if(tipo===1){
    this.estado="aceptado";
    this.id_cotizacion=id_c;
   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.solicitudCliente.putsoli(id_solicitud,this.estado).subscribe((res)=>{
      window.location.reload();
    });

    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
    
  }else if(tipo===2){
    this.estado="rechazado";
    this.id_cotizacion=id_c;

    this.solicitudCliente.putsoli(id_solicitud,this.estado).subscribe((res)=>{
      window.location.reload();
    });

   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
  }else if(tipo===3){

    this.estado="contraofertado";
    this.id_cotizacion=id_c;

    this.solicitudCliente.putsoli(id_solicitud,this.estado).subscribe((res)=>{
      window.location.reload();
    });

   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
  }
 
  
}

mostrarNombrelogueado(){
  console.log("marcosasantosa1998@gmail.com");
     return this.usuario;
}

contador:number=0;
mostrarcotiazaciones(){

if(this.contador==0){ 


  this.cotizaiones=true;
  this.contador=this.contador+1;
}else{
  this.cotizaiones=false;
  this.contador=0;
}
}

}
