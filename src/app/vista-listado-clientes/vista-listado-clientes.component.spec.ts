import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaListadoClientesComponent } from './vista-listado-clientes.component';
import { LoginService}  from '../services/login.service';
import {AceptarCotizacionService }  from '../services/aceptar-cotizacion.service';
import { Router } from '@angular/router';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from "@angular/router/testing";

import {login} from "../models/login.interface"

describe('VistaListadoClientesComponent', () => {
  let component: VistaListadoClientesComponent;
  let fixture: ComponentFixture<VistaListadoClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VistaListadoClientesComponent] ,
      imports: [
        HttpClientTestingModule,
        ApolloTestingModule,
       FormsModule,
       RouterTestingModule,
       
      ],
      
    })
    .compileComponents();
  }));
  
  beforeEach(() => {
    fixture = TestBed.createComponent(VistaListadoClientesComponent);
   
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('El usuario lograr acceder', () => {
    
    expect(component).toBeTruthy();
  });

  it('Acceso del usuario cliente', () => {
    let user:login={  jwt:"dsad",
      user:
      {
        id: "1",
        username: "marcos",
        email: "marcosasantosa1998@gmail.com",
        provider: "no",
        confirmed: "yes",
        blocked: "no",
        tipo:"cliente"
      }
      };
   component.listado.setUser(user);
    expect(component).toBeTruthy();
  });

    it('Se puede dar clic sobre el boton aceptar', () => {
    expect(<HTMLButtonElement>document.getElementById('aceptar')).toBeNull();
  });


});
