import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {HttpClient} from '@angular/common/http';
import { ArticlesComponent } from './articles.component';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
describe('ArticlesComponent', () => {
  let component: ArticlesComponent;
  let fixture: ComponentFixture<ArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesComponent ] ,
      imports: [
        HttpClientTestingModule,
        ApolloTestingModule,
       FormsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('La pagina deberia de cargar sin problemas', () => {
    expect(component).toBeTruthy();
  });

  it('Se puede dar clic sobre el boton iniciar sesión', () => {
    expect(<HTMLButtonElement>document.getElementById('inicia')).toBeTruthy();
  });
});
