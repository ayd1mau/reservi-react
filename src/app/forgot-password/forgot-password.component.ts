import { Component, OnInit } from '@angular/core';
import { MailService } from '../services/mail.service'
import { AuthService } from '../services/auth.service'

import {mail} from '../models/mail.interface';
import {login} from '../models/login.interface';
import { flatMap } from 'rxjs/operators';

import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})

export class ForgotComponent implements OnInit {

  correo:string ="";
  contrasena:string ="";
  contrasenaconfirm:string ="";
  token:string ="";
  emailform:boolean=true;
  passwordform:boolean=false;

  constructor(public mailService: MailService,public authService: AuthService,private route:Router, private activeroute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.token = this.activeroute.snapshot.paramMap.get("t");
    this.passwordform = !!this.token
    this.emailform = !this.passwordform;
  }

  enviar_correo(){
    this.mailService.send(this.correo).subscribe((res:mail)=>{
      alert("Se ha enviado correo con información para restaurar contraseña")
      
      this.correo="";
      this.contrasena="";
      this.contrasenaconfirm="";
    },
    error=>{
      this.correo="";
      this.contrasenaconfirm="";
      this.contrasena="";
    });
  }

  guardarcontrasena(){
    this.authService.resetPassword(this.token,this.contrasena,this.contrasenaconfirm).subscribe((res:login)=>{
      alert("Se ha actualizado la contraseña correctamente")      

      this.route.navigate(['login']);
    },
    error=>{
      this.correo="";
      this.contrasena="";
      this.contrasenaconfirm="";
    });
  }

  verificar_credenciales(){

    
  }
}