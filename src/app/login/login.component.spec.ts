import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import {HttpClient} from '@angular/common/http';
import {LoginService} from '../services/login.service';
import { RouterTestingModule } from "@angular/router/testing";
import {Router} from '@angular/router';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { httpstatus } from 'aws-sdk/clients/glacier';

describe('SolicitarAyudaComponent', () => {
  let component:LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let AyudaService;
  let ayuda_egistro;
  let http;
  let app: LoginComponent;
  let router;

  
       
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent],
      imports:[HttpClientTestingModule,FormsModule,RouterTestingModule],
      providers:[LoginService],
      
     
    })
    .compileComponents();
  }));


  beforeEach(() => {
   fixture = TestBed.createComponent(LoginComponent);
   component = fixture.componentInstance;
   fixture.detectChanges();
   http=HttpClient;
   AyudaService=  LoginService;
   ayuda_egistro= Router;
   
   app=new LoginComponent(AyudaService,ayuda_egistro);
  });

  it('La pagina debería de cargar correctamente, se puede visualizar los campos ', () => {
    expect(app).toBeTruthy();
  });

  
   


  it('Se ingresan las credenciales',async(()=>{
    (<HTMLInputElement>document.getElementById('correo')).value='marcosasantosa1998@gmail.com';
    (<HTMLInputElement>document.getElementById('password')).value='holamundo';
   
    expect( (<HTMLInputElement>document.getElementById('password')).value).toEqual("holamundo");
    expect( (<HTMLInputElement>document.getElementById('correo')).value).toEqual("marcosasantosa1998@gmail.com");
    


  }))

    
it('debería aprobar credenciales',()=>{
  app.correo="marcosasantosa1998@gmail.com";
  app.contrasena="holamundo";  
  
  expect("http://localhost:1337/auth/local").toBeTruthy();
})





  
});
