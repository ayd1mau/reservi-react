import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdituserAdminComponent } from './edituser-admin.component';
import { ToastService } from '../services/-toast.service';
import { AprobarCurriculumService } from '../services/aprobar-curriculum.service';
import { proveedor } from '../models/proveedor.interface'

describe('EdituserAdminComponent', () => {
  let component: EdituserAdminComponent;
  let fixture: ComponentFixture<EdituserAdminComponent>;
  let service: AprobarCurriculumService;
  let toast: ToastService;
  let spy: any;
  let auxUsers: proveedor[];

  const dummyGet: proveedor = {
    id: 9,
    username: 'Fathom',
    email: 'diegocaballeros@gmail.com',
    provider: 'local',
    confirmed: true,
    blocked: null,
    role:{
        id: 1,
        name: 'Authenticated',
        description: 'Default role given to authenticated user.',
        type: 'authenticated',
        created_by: null,
        updated_by: null,
    },
    created_by: null,
    updated_by: null,
    created_at: '2020-08-28T05:12:55.688Z',
    updated_at: '2020-08-28T05:12:56.548Z',
    direccion: 'VillaNueva, Zona6',
    telefono: '30963906',
    tipo: 'cliente',
    nombre: 'Diego',
    apellido: 'Caballeros',
    documento: null
  };

  beforeEach(async(() => {
    component = new EdituserAdminComponent(service,toast);
    auxUsers = [];
    auxUsers.push(dummyGet)
  }));

  afterEach(async(() => {
    component = null;
    spy = null;
  }));

  it('Prueba para verificar la filtracion de usuarios correctamente', async() => {

    spy = spyOn(component,'getUsuarios').and.returnValue(auxUsers);
    component.nombre = 'Diego';
    component.filtrarUsers();
    expect(component.getUser()).toEqual(auxUsers);
    expect(component.getUser()[0].nombre === 'Gustavo').toBeFalsy()
  });

});
