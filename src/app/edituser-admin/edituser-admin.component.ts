import { Component, OnInit } from '@angular/core';

import { AprobarCurriculumService } from '../services/aprobar-curriculum.service'
import { proveedor } from '../models/proveedor.interface';
import { ToastService } from '../services/-toast.service';

@Component({
  selector: 'app-edituser-admin',
  templateUrl: './edituser-admin.component.html',
  styleUrls: ['./edituser-admin.component.css']
})
export class EdituserAdminComponent implements OnInit {


  private usuarios: proveedor[] =[]; 
  private posiblesUsuarios: proveedor[]=[];

  public nombre: String;

  // VARIABLES PARA MOSTRAR EL DIV DEL FORMULARIO PARA CADA USUARIO/PROFESIONAL
  formData: boolean = false;

  // VARIABLES PARA RECOLECTAR INFORMACION
  name: string = "";
  lastname: string = "";
  username: string = "";
  numberPhone: number;
  direction: string = "";
  email: string = "";
  
  idUser: Number = 0;

  constructor(public aprove: AprobarCurriculumService, public toastService: ToastService) { }

  ngOnInit() {
    this.ObtenerUsuarios();
  }

   // CONSUMIR API PARA RECUPERAR DATOS
 ObtenerUsuarios(): void{
  this.aprove.getProvedores().subscribe((res: proveedor[])=>{
    this.usuarios = [];
    this.usuarios = res;
    console.log('Antes de quitar elementos')
    console.log(this.usuarios)
    //this.filtrarUsers();
    //console.log('Despues de quitar elementos')
    //console.log(this.usuarios)
  },
  error=>{
    console.log('Fallo')
    this.usuarios = [];
  }
  );
 }

  getUser(): proveedor[]{
    return this.posiblesUsuarios;
  }

  getUsuarios(): proveedor[]{
    return this.usuarios;
  }

  public filtrarUsers(): void{
    this.posiblesUsuarios = [];
    this.getUsuarios().map((user)=>{
      if(user.nombre != null){
        if(user.nombre.toLowerCase() === this.nombre.toLowerCase()){
          this.posiblesUsuarios.push(user);
        }
      }
    });
    // VERIFICO SI ENCONTRO ALGUN NOMBRE
    if(!this.posiblesUsuarios.length){
      this.toastService.showInfo("No hay ningun nombre que coincida con "+this.nombre);
    }
    
  }

  public showData(user: proveedor): void{
    this.formData = true;
    this.idUser = user.id;

    this.name = user.nombre.toString();
    this.lastname = user.apellido.toString();
    this.username = user.username.toString();
    this.numberPhone = parseInt(user.telefono.toString());
    this.direction = user.direccion.toString();
    this.email = user.email.toString();
  }

  public ActualizarDatos(): void{
    this.aprove.ActualizarDatos(this.idUser,this.name,this.lastname,this.username,
                this.numberPhone.toString(),this.direction,this.email).subscribe( async (res)=>{

                this.toastService.showSucess("Se actualizaron los datos del usuario correctamente!!");
                this.ObtenerUsuarios();
                this.posiblesUsuarios = []
                this.idUser = 0;
                this.formData = false;
    },error=>{
        this.toastService.showError("Hubo un error con la actualizacion de datos");
    });

  }

  // METODO PARA VALIDAR QUE SOLO INGRESE NUMEROS PARA EL TELEFONO
  validarNumero(event: any){
    const pattern = /[0-9\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }


}
