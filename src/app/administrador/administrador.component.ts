import { Component, OnInit } from '@angular/core';

import { AprobarCurriculumService } from '../services/aprobar-curriculum.service'
import { proveedor } from '../models/proveedor.interface';
import { ToastService } from '../services/-toast.service';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {

 //src = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
 src = 'https://bucket-reservi.s3.us-east-2.amazonaws.com/sorry-convertido.pdf';
 private usuarios: proveedor[] =[]; 
  
 datosUsuario: proveedor = null;

  constructor(public aprove: AprobarCurriculumService, public toastService: ToastService ) { }

  ngOnInit() {
    this.ObtenerUsuarios();
  }

  // CONSUMIR API PARA RECUPERAR DATOS
 ObtenerUsuarios(): void{
  this.aprove.getProvedores().subscribe((res: proveedor[])=>{
    this.usuarios = res;
    console.log('Antes de quitar elementos')
    console.log(this.usuarios)
    this.filtrarUsers();
    console.log('Despues de quitar elementos')
    console.log(this.usuarios)
  },
  error=>{
    console.log('Fallo')
    this.usuarios = [];
  }
  );
 }

 // FILTRAR LOS USUARIOS
 filtrarUsers(): void{
    let aux = this.usuarios;
    for(let user of aux){
      if(!this.isProviderNoAuthorized(user))
        this.removeUser(user);
    }
 }
 
 // ANALIZAR PARA VER SI SE MUESTRA
  isProviderNoAuthorized(user: proveedor): boolean{
    if(user.tipo === null || user.documento == null || user.nombre == null)
      return false;

    /*if(user.tipo === 'proveedor'){
      if(user.confirmed == null || !user.confirmed)
      return true;
    }*/
    return user.tipo === 'proveedor' && !user.confirmed;
    //return false;
  }

  // REMOVER USUARIOS DE LA LISTA
  removeUser(user: proveedor): void{
    let index = this.usuarios.indexOf(user);
    this.usuarios.splice(index,1);
  }

  // AUTORIZAR PROVEEDOR
  authorizeProvider(): void{
    if(this.datosUsuario === null){
      this.showError('Error!! No ha seleccionando a ningun usuario para aprobarlo');
      return;
    }
  
    this.aprove.putProveedores(this.datosUsuario.id).subscribe((res)=>{
      console.log(res)
      this.showSucess('Felicidades!! acaba de autorizar a un nuevo Proveedor!!');
      this.src = 'https://bucket-reservi.s3.us-east-2.amazonaws.com/sorry-convertido.pdf';
      this.datosUsuario = null;
      console.log('Funciono!!')
    },
    error=>{
      console.log(error)
      console.log('Dio error!!')
      return;
    });
    this.removeUser(this.datosUsuario);
    this.datosUsuario = null;
  }
  
  
  // ELIMINAR BUFFER DE USUARIO
  deleteProvider(): void{
    this.src = 'https://bucket-reservi.s3.us-east-2.amazonaws.com/sorry-convertido.pdf';
    this.datosUsuario = null;
    this.showInfo('Ya no tiene ningun usuario seleccionado, escoja uno!!');
    
  }
  
  // TOAST PARA QUE ALGO SALIO BIEN
  showSucess(message: string): void{
    this.toastService.show(message, {
      classname: 'bg-success text-light',
      delay: 4000 ,
      autohide: true,
      headertext: 'Sucess!!'
    });
  }
  
  // TOAST PARA QUE ALGO SALIO MAL
  showError(message: string): void{
    this.toastService.show(message, {
      classname: 'bg-danger text-light',
      delay: 4000 ,
      autohide: true,
      headertext: 'Error!!!'
    });
  }
  
  // TOAST INFORMATIVO
  showInfo(message: string): void{
    this.toastService.show(message, {
      classname: 'bg-info text-light',
      delay: 4000,
      autohide: true,
      headertext: 'Atencion!!!'
    });
  }
  
  
  
  getUser(): proveedor[]{
    return this.usuarios;
  }

  setUser(nuevo: proveedor[]): void{
    this.usuarios = nuevo;
  }

  addUser(nuevo: proveedor): void{
    this.usuarios.push(nuevo);
  }

  getUrlPdf(): String{
    return this.src;
  }

  setUrlPdf(nuevo: string): void{
    this.src = nuevo;
  }

  saveStatus(nuevo: string, aux: proveedor): void{
    this.setUrlPdf(nuevo);
    this.datosUsuario = aux;
  }
}
