import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor() { }

  toasts: any[] = [];

  // Push new Toasts to array with content and options
  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  // Callback method to remove Toast DOM element from view
  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  // TOAST PARA QUE ALGO SALIO BIEN
  showSucess(message: string): void{
    this.show(message, {
      classname: 'bg-success text-light',
      delay: 4000 ,
      autohide: true,
      headertext: 'Sucess!!'
    });
  }

  // TOAST PARA QUE ALGO SALIO MAL
  showError(message: string): void{
    this.show(message, {
      classname: 'bg-danger text-light',
      delay: 4000 ,
      autohide: true,
      headertext: 'Error!!!'
    });
  }

  // TOAST INFORMATIVO
  showInfo(message: string): void{
    this.show(message, {
      classname: 'bg-info text-light',
      delay: 4000,
      autohide: true,
      headertext: 'Atencion!!!'
    });
  }
  
}
