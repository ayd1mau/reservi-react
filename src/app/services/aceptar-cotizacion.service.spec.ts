import { TestBed } from '@angular/core/testing';

import { AceptarCotizacionService } from './aceptar-cotizacion.service';

describe('AceptarCotizacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AceptarCotizacionService = TestBed.get(AceptarCotizacionService);
    expect(service).toBeTruthy();
  });
});
