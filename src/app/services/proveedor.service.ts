import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ProveedorService {
  API_URI = "http://localhost:1337";
  constructor(private http: HttpClient) { }


  buscarTodos(){
    return this.http.get(`${this.API_URI}/solicituds`);
  }

  buscarUno(id:string){
    return this.http.get(`${this.API_URI}/solicituds/`+id);
  }

  setCotizacion(cotiz:any){
    return this.http.post(`${this.API_URI}/cotizacions`,cotiz);
  }


}
