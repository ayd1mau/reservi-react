import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import {map} from 'rxjs/operators'
import * as moment from 'moment';
import {Clientes} from '../models/Clientes.interface';
@Injectable({
  providedIn: 'root'
})
export class SolicitudService {

  constructor(private http:HttpClient) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })
  // METODO POST PARA CREAR SOLICITUD DE SERVICIO
  post(titulo:string, descripcion:string, direccion_servicio:string, categorias:Number[],cliente:Clientes){
    const url = "http://localhost:1337/solicituds";

    return this.http.post(url,{titulo, descripcion, direccion_servicio,user:cliente.user,
      fecha: moment(new Date()).format('YYYY-MM-DD'), estado:"espera", categorias} );
  }

//obtiene la solictud
  getSolicitud(correo:String){
    const url = "http://localhost:1337/solicituds/aux/"+correo;
    return this.http.get(url);
  }


  putsoli(id:String,estado:String){
    const url = "http://localhost:1337/solicituds/"+id;
    
   return this.http.put(url,{"estado": estado},{headers:this.headers});
  }

}
