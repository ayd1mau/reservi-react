import { TestBed } from '@angular/core/testing';

import { SolicitarAyudaService } from './solicitar-ayuda.service';

describe('SolicitarAyudaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SolicitarAyudaService = TestBed.get(SolicitarAyudaService);
    expect(service).toBeTruthy();
  });
});
