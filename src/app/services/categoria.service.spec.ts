import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CategoriaService } from './categoria.service';
import { of } from 'rxjs'; // Add import

describe('CategoriaService', () => {
  let categoriaService: CategoriaService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        CategoriaService
      ],
    });

    categoriaService = TestBed.get(CategoriaService);
    httpMock = TestBed.get(HttpTestingController);
  });

  describe('all', () => {
    it('Obtener listado de categorias', () => {
      const categorias = [
        {
          id: '1',
          nombre: 'Carpintería',
        },
        {
          id: '2',
          nombre: 'Plomeria',
        }
      ];
      let response;
      spyOn(categoriaService, 'get').and.returnValue(of(categorias));
  
      categoriaService.get(null,{}).subscribe(res => {
        response = res;
      });
  
      expect(response).toEqual(categorias);
    });
  });
});
