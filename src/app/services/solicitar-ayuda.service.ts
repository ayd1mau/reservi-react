import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ayuda_body} from  '../models/ayuda.interface';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class SolicitarAyudaService {

  constructor(private http:HttpClient) {

   }

   headers: HttpHeaders= new HttpHeaders({"Content-Type":"application/json"})

 
 
   agregarAyuda(nombre:String,apellido:String,correo:String,telefono:number,descrip:String){
   
    const url="http://localhost:1337/ayudas/";

    return this.http.post(url,
      {
        "nombre":nombre,
        "apellido":apellido,
        "email":correo,
        "phone":telefono,
        "descripcion_problema":descrip,
        "estado_duda":"enproceso"
      },{headers:this.headers});
  }
 
  }


 
