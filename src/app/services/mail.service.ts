import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { LoginService }  from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private http:HttpClient,public login:LoginService) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })
  // METODO POST PARA CREAR SOLICITUD DE SERVICIO
  send(email:string){
    const url = `http://localhost:1337/categorias/forgotPassword`;
    return this.http.post(url, {email},{headers:{}});
  }
}