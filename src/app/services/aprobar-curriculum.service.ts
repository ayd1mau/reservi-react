import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AprobarCurriculumService {

  constructor(private http:HttpClient) { }

  getProvedores(){
    const url = "http://localhost:1337/users"
    return this.http.get(url)
  }

  putProveedores(id: Number){
    const url = "http://localhost:1337/users/"+id;
    return this.http.put(url, {'confirmed':true} );
  }
  getFlag(){
    return false;
  }

  public ActualizarDatos(id: Number, nombre: string, apellido: string, username: string, 
                          telefono: string, direccion: string, email: string){
    const url = "http://localhost:1337/users/"+id;
    return this.http.put(url,{'nombre': nombre, 'apellido': apellido, 'username': username, 
                              'telefono': telefono, 'direccion':direccion, 'email':email});
  }
}
