import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthService } from './auth.service';
import { of } from 'rxjs'; // Add import

describe('authService', () => {
  let authService: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        AuthService
      ],
    });

    authService = TestBed.get(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });

  describe('all', () => {
    it('Resetear password', () => {
      const password = {
        jwt:'asdasdads',
        user:{
          id:'2123121'
        }
      };
      let response;
      spyOn(authService, 'resetPassword').and.returnValue(of(password));
  
      authService.resetPassword('asdasdsa','test@ejemplo.com','test@ejemplo.com').subscribe(res => {
        response = res;
      });
  
      expect(response).not.toBeFalsy(response);
    });
  });
});
