import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { LoginService }  from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private http:HttpClient,public login:LoginService) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })
  // METODO POST PARA CREAR SOLICITUD DE SERVICIO
  get(id:string, params:{}){
    const url = `http://localhost:1337/categorias${id||''}`;
    let user = this.login.getUseraux() || {};
    return this.http.get(url, {headers:{Authorization: `Bearer ${user.jwt}`}});
  }
}
