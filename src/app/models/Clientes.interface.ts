export interface Clientes {
  jwt:String, 
  user:
    {
      id: String,
      username: String,
      email: String,
      provider: String,
      confirmed: String,
      blocked: String,
      tipo:String;
    }
}