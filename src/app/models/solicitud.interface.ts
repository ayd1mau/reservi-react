
export interface Solicitud{
     id: number,
     titulo: String,
     descripcion: String,
     direccion_servicio: String,
     fecha:String,
     estado: String
     cotizacions:{
          id: Number,
          precio: Number,
          comentario: String,
          fecha: String,
          estado: String,
          solicitud: Number,
          user: Number
     }
     /*agregar los demas endpoints */
}