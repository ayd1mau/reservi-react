
import { browser, by, element  } from 'protractor';

export class vista_proveedor {
  navigateTo() {
    return browser.get('/inicio-proveedor') as Promise<any>;
  }

  getTitleText() {
    return element(by.id('cerrar')).getText() as Promise<string>;
   
  }

  versolicitudes(){
    return element(by.id('vers')).click();      
}

getBotonaceptarText() {
  return element(by.id('aceptar')).getText() as Promise<string>;
 
}

getBotonrechaText() {
  return element(by.id('rechazar')).getText() as Promise<string>;
 
}
getBotonconText() {
  return element(by.id('contra')).getText() as Promise<string>;
 
}
}