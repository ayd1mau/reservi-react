import { browser, by, element  } from 'protractor';

export class Cotizacion {
  navigateTo() {
    return browser.get('/enviar-cotizacion/2') as Promise<any>;
  }

  setPrecio() {
    element(by.id('id_precio')).sendKeys('100.00');
   
  }
  setFecha() {
    element(by.id('id_fecha')).sendKeys('2020-10-22');
   
  }
  setComentario() {
    element(by.id('id_comentario')).sendKeys('para test');
   
  }
  enviar(){
      return element(by.id('id_enviar')).click();      
  }
}

