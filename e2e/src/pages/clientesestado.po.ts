import { browser, by, element  } from 'protractor';

export class vista_cliente {
  navigateTo() {
    return browser.get('/vista-listado-cliente') as Promise<any>;
  }

  getTitleText() {
    return element(by.id('cerrar')).getText() as Promise<string>;
   
  }

  vercotizaciones(){
    return element(by.id('ver')).click();      
}

getBotonaceptarText() {
  return element(by.id('aceptar')).getText() as Promise<string>;
 
}

getBotonrechaText() {
  return element(by.id('rechazar')).getText() as Promise<string>;
 
}
getBotonconText() {
  return element(by.id('contra')).getText() as Promise<string>;
 
}
  /*setPrecio() {
    element(by.id('id_precio')).sendKeys('100.00');
   
  }
  setFecha() {
    element(by.id('id_fecha')).sendKeys('2020-10-22');
   
  }
  setComentario() {
    element(by.id('id_comentario')).sendKeys('para test');
   
  }
  enviar(){
      return element(by.id('id_enviar')).click();      
  }*/
}

