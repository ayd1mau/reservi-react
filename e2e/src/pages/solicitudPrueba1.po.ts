import { browser, by, element  } from 'protractor';

export class PruebaLogin {
  navigateTo() {
    return browser.get('/login') as Promise<any>;
  }

  exitTo() {
    return browser.quit() as Promise<any>;
  }

  setUsuario(user:string) {
    element(by.id('usuario')).sendKeys(user);
    browser.sleep(5000);
  }
  setContraseña(pass:string) {
    element(by.id('password')).sendKeys(pass);
    
    browser.sleep(5000);
   
  }

  cliclogin(){
    element(by.id('regis')).click();   
    browser.sleep(10000);  
    browser.getCurrentUrl();
    browser.waitForAngularEnabled();
  }

getTitleText() {
  return element(by.id('inic')).getText() as Promise<string>;
 
}
 /* setComentario() {
    element(by.id('id_comentario')).sendKeys('para test');
   
  }
  enviar(){
      return element(by.id('id_enviar')).click();      
  }*/
}