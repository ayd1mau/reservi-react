module.exports = {
  apps : [
    {
      name   : "ReserviReact",
      script : "npm",
      args : "start",
      instances: 1,
      exec_mode : "cluster",
      env: {
        "NODE_ENV": "dev"
      }
    }
  ]
}
